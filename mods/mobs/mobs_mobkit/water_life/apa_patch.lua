 
water_life.poison_after_death = minetest.settings:get_bool("water_life_poison_after_death") or false

minetest.register_on_dieplayer(function(player)
	if not player then return end
	if water_life.poison_after_death then return end

	local meta=player:get_meta()
	meta:set_int("snakepoison",0)
	water_life.change_hud(player,"poison",0)
end)