 #!/bin/bash

LIB="$(realpath $(dirname $0))"  # Absolute path
PROJ="$(dirname $LIB)"           # Subgame dir (..)

#******************************************************************************

# Mods

SRC="$(dirname $PROJ)"/mods_src/ # mods sources (../..)
DST="$PROJ"/mods/                # Subgame mods
LOG="$PROJ"/mod_sources.txt
source "$LIB"/build-whynot.lib
cd "$SRC" # for proper resolving the '*'

## Sync minetest_game
mod_install minetest_game --exclude=farming --exclude=bucket --exclude=doors minetest_game/mods/*

# Install all other mods
mod_install blocks
mod_install buildings
mod_install environment
mod_install flora
mod_install gui
mod_install lib_api
mod_install mobs/mobs_mobs
mod_install mobs/mobs_mobkit
mod_install player --exclude=3d_armor/3d_armor_ip/ --exclude=3d_armor/3d_armor_ui/
mod_install tools

#******************************************************************************

# Textures

SRC="$(dirname $PROJ)"/texture_src/ # mods sources (../..)
DST="$PROJ"/textures/                # Subgame mods
LOG="$PROJ"/textures_sources.txt
source "$LIB"/build-whynot.lib
cd "$SRC" # for proper resolving the '*'

cd "$SRC" # for proper resolving the '*'

mod_install LessDirt --exclude=ethereal --exclude=mcl_core

#******************************************************************************
